import random
#create list of 100 random numbers from 0 to 1000
res = random.sample(range(0, 1000), 100)
#create variables for further useage
evens_sum = 0
evens_count = 0
odds_sum = 0
odds_count = 0
res_sorted = []
#create for loop to go through each element in list
for i in range(0, len(res)):
    #get minimal num in list
    x = min(res)
    #insert it to new list in increasing order
    res_sorted.append(x)
    #and delete it from old list
    res.remove(x)
    
    #check if number is even
    if x%2==0:
        #add even numbers
        evens_sum +=x
        #count even numbers
        evens_count +=1
    #if number is not even, so it is odd
    else:
        #add odd numbers
        odds_sum +=x
        #count odd numbers
        odds_count+=1
#print average of even numbers
print(evens_sum/evens_count)
#print average of odd numbers
print(odds_sum/odds_count)
